/**
 * filename : i2c.c
 *
 * created  : 2017/03/11
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : I2C communication
 */

#include "i2c.h"

#include <avr/io.h>

static uint8_t i2c_errors_count = 0;

static void __attribute__((noinline)) i2c_trans_and_check(uint8_t twcr)
{
    TWCR = twcr;
    uint16_t count = I2C_TIME_OUT;
    while (!(TWCR & _BV(TWINT)))
    {
        --count;
        if (count == 0)
        {
            TWCR = 0;
            ++i2c_errors_count;
            break;
        }
    }
}

void i2c_start_SLA_W(uint8_t addr)
{
    // START condition
    // Slave Address + write bit
    i2c_trans_and_check(_BV(TWINT) | _BV(TWSTA) | _BV(TWEN));
    i2c_trans_data(addr << 1);
}

void i2c_start_SLA_R(uint8_t addr)
{
    //START condition
    //Slave Address + read bit
    i2c_trans_and_check(_BV(TWINT) | _BV(TWSTA) | _BV(TWEN));
    i2c_trans_data((addr << 1) | 1);
}

void i2c_stop(void)
{
    TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
}

void i2c_trans_data(uint8_t data)
{
    TWDR = data;
    i2c_trans_and_check(_BV(TWINT) | _BV(TWEN));
}

uint8_t i2c_receive_data(void)
{
    //return ACK
    i2c_trans_and_check(_BV(TWINT) | _BV(TWEA) | _BV(TWEN));
    return TWDR;
}

uint8_t i2c_last_receive_data(void)
{
    //retrun NACK
    i2c_trans_and_check(_BV(TWINT) | _BV(TWEN));
    uint8_t twdr = TWDR;
    i2c_stop();
    return twdr;
}

void i2c_begin(uint32_t bit_rate)
{
    // prescaler = 1
    // I2C enable
    TWSR = 0;
    TWBR = ((F_CPU / bit_rate) - 16) / 2;
    TWCR = 1 << TWEN;
}

void i2c_write_REG(uint8_t addr, uint8_t regaddr, uint8_t data)
{
    i2c_start_SLA_W(addr);
    i2c_trans_data(regaddr);
    i2c_trans_data(data);
    i2c_stop();
}

uint8_t i2c_read_REG(uint8_t addr, uint8_t regaddr)
{
    i2c_start_SLA_W(addr);
    i2c_trans_data(regaddr);
    i2c_start_SLA_R(addr);
    uint8_t data = i2c_last_receive_data();
    return data;
}

void i2c_read_multi_REG(uint8_t addr, uint8_t regaddr, uint8_t *buffer, uint8_t count)
{
    i2c_start_SLA_W(addr);
    i2c_trans_data(regaddr);
    i2c_start_SLA_R(addr);
    for (uint8_t i = 0; i < count - 1; ++i)
    {
        *buffer++ = i2c_receive_data();
    }
    *buffer = i2c_last_receive_data();
}

uint8_t i2c_get_errors_count(void)
{
    return i2c_errors_count;
}
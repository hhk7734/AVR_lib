/**
 * filename : i2c.h
 * 
 * created  : 2017/03/11
 * 
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 * 
 * purpose : I2C communication
 */

#ifndef _I2C_H_
#define _I2C_H_
#ifdef __cplusplus
extern "C"
{
#endif

#include <inttypes.h>

#if defined(__AVR_ATmega328P__)
#else
    #error "maybe unsupported device"
#endif

#ifndef F_CPU
    #error "F_CPU is not defined"
#endif

#define I2C_TIME_OUT      255
#define I2C_STANDARD_MODE 100000L
#define I2C_FAST_MODE     400000L

void    i2c_start_SLA_W(uint8_t addr);
void    i2c_start_SLA_R(uint8_t addr);
void    i2c_stop(void);
void    i2c_trans_data(uint8_t data);
uint8_t i2c_receive_data(void);
uint8_t i2c_last_receive_data(void);

void    i2c_begin(uint32_t bit_rate);
void    i2c_write_REG(uint8_t addr, uint8_t regaddr, uint8_t data);
uint8_t i2c_read_REG (uint8_t addr, uint8_t regaddr);
void    i2c_read_multi_REG (uint8_t addr, uint8_t regaddr, uint8_t *buffer, uint8_t count);
uint8_t i2c_get_errors_count(void);

#ifdef __cplusplus
}
#endif

#endif // _I2C_H_
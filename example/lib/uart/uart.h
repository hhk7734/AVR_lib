/**
 * filename : uart.h
 * 
 * created  : 2017/03/07
 * edited   : 2017/03/15
 * edited   : 2018/09/08
 * 
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 * 
 * purpose : uart communication
 */

#ifndef _UART_H_
#define _UART_H_
#ifdef __cplusplus
extern "C"
{
#endif

#include <inttypes.h>

#define _UART0_
// #define _UART0PRINT_

#ifdef _UART0_
#define UART0_RX_BUF_SIZE 64 //must be power of 2
#define UART0_TX_BUF_SIZE 64
#endif

#if defined(__AVR_ATmega328P__)
#else
#error "maybe unsupported device"
#endif // __AVR_ATmega328P__

#ifndef F_CPU
#error "F_CPU is not defined"
#endif // F_CPU

#ifdef _UART0_
void    uart0_begin(uint32_t baud_rate);
uint8_t uart0_read(void);
void    uart0_write(uint8_t data);
void    uart0_print_str(char *str);
uint8_t uart0_available(void);
void    uart0_flush(void);
void    uart0_print_u16(uint16_t data);
void    uart0_print_i16(int16_t data);

#ifdef _UART0PRINT_
void    uart0_print_u32(uint32_t data);
void    uart0_print_i32(int32_t data);
void    uart0_print_u64(uint64_t data);
void    uart0_print_i64(int64_t data);
void    uart0_print_f(float data, int digits);
#endif // _UART0PRINT_
#endif // _UART0_

#ifdef __cplusplus
}
#endif

#endif /* _UART_H_ */

/**
 * filename : tc_time.h
 *
 * created  : 2017/03/10
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : time, micros, millis
 */

#ifndef _TC_TIME_H_
#define _TC_TIME_H_
#ifdef __cplusplus
extern "C"
{
#endif

#include <inttypes.h>

#if defined(__AVR_ATmega328P__)
#else
    #error "maybe unsupported device"
#endif // __AVR_ATmega328P__

#ifndef F_CPU
    #error "F_CPU is not defined"
#elif F_CPU != 16000000UL
    #error "F_CPU is not 16MHz, don't use this code."
#endif // F_CPU

void     tc_time_setup(void);
uint32_t micros(void);
uint32_t millis(void);

#ifdef __cplusplus
}
#endif

#endif // _TC_TIME_H_
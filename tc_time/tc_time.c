/**
 * filename : tc_time.c
 *
 * created  : 2017/03/10
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : time, micros, millis
 */

#include"tc_time.h"

#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint32_t tc_isr_count = 0;
volatile uint32_t tc_isr_millis = 0;
volatile uint8_t tc_remainder_8micros = 0;

ISR(TIMER2_OVF_vect)
{
    ++tc_isr_count;

    ++tc_isr_millis;
    tc_remainder_8micros += 3; // 8*3 == 24us
    if (tc_remainder_8micros > 124) // 8*125 == 1000us
    {
        ++tc_isr_millis;
        tc_remainder_8micros -= 125;
    }
}

void tc_time_setup(void)
{
    TCCR2A = 0;
    TCCR2B = _BV(CS22); // prescaler == 64

    TIMSK2 = _BV(TOIE2);
    sei();
}

uint32_t micros(void)
{
    uint16_t tcnt;
    uint32_t count;

    cli();

    count = tc_isr_count;
    tcnt = TCNT2;

    // after cli();, if TOV2 was set, ++count;
    if((TIFR2 & _BV(TOV2)) && tcnt < 32) ++count;

    sei();

    return (count<<10) + (tcnt<<2);
}

uint32_t tc_millis(void)
{
    return tc_isr_millis;
}
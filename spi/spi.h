/**
 * filename : spi.h
 *
 * created  : 2018/02/11
 * edited   : 2018/07/21
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : SPI communication
 */

#ifndef _SPI_H_
#define _SPI_H_
#ifdef __cplusplus
extern "C"
{
#endif

#include <inttypes.h>

#if defined(__AVR_ATmega328P__)
#else
    #error "maybe unsupported device"
#endif // __AVR_ATmega328P__

#ifdef SPI_INTERVAL
    #define SPI_INTERVAL_TIME_US 10
#endif // SPI_INTERVAL

/*
 * speed == clock/(2^div)
 */
#define SPI_SCK_DIV_2   2
#define SPI_SCK_DIV_4   4
#define SPI_SCK_DIV_8   8
#define SPI_SCK_DIV_16  16
#define SPI_SCK_DIV_32  32
#define SPI_SCK_DIV_64  64
#define SPI_SCK_DIV_128 128

#define SPI_TIME_OUT    255

uint8_t spi_data_RW(uint8_t data);
void    spi_data_multi_RW(uint8_t *buf, uint8_t count);

void    spi_begin(uint8_t clock_divider);
void    spi_write_REG(volatile uint8_t *port, uint8_t pin, uint8_t regaddr, uint8_t data);
uint8_t spi_read_REG(volatile uint8_t *port, uint8_t pin, uint8_t regaddr);
void    spi_read_multi_REG(volatile uint8_t *port, uint8_t pin, uint8_t regaddr, uint8_t *buffer, uint8_t count);
uint8_t spi_get_errors_count(void);

#ifdef __cplusplus
}
#endif

#endif // _SPI_H_
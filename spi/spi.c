/**
 * filename : spi.c
 * 
 * created  : 2018/02/11
 * edited   : 2018/07/21
 * 
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 * 
 * purpose : SPI communication
 */

#include "spi.h"

#include <avr/io.h>
#include <util/delay.h>

static uint8_t spi_errors_count = 0;

uint8_t spi_data_RW(uint8_t data)
{
    SPDR = data;
    uint16_t time_out = SPI_TIME_OUT;
    while (!(SPSR & _BV(SPIF)))
    {
        --time_out;
        if (time_out == 0)
        {
            ++spi_errors_count;
            break;
        }
    }
    return SPDR;
}

void spi_data_multi_RW(uint8_t *buf, uint8_t count)
{
    SPDR = *buf;
    uint16_t time_out;
    while (--count)
    {
        uint8_t out = *(buf + 1);
        time_out = SPI_TIME_OUT;
        while (!(SPSR & _BV(SPIF)))
        {
            --time_out;
            if (time_out == 0)
            {
                ++spi_errors_count;
                break;
            }
        }
        uint8_t in = SPDR;
#ifdef SPI_INTERVAL
        _delay_us(SPI_INTERVAL_TIME_US);
#endif
        SPDR = out;
        *buf++ = in;
    }
    time_out = SPI_TIME_OUT;
    while (!(SPSR & _BV(SPIF)))
    {
        --time_out;
        if (time_out == 0)
        {
            ++spi_errors_count;
            break;
        }
    }
    *buf = SPDR;
}

void spi_begin(uint8_t clock_divider)
{
    DDRB |= _BV(DDB5) | _BV(DDB3); // MOSI, SCK -- output
    DDRB &= ~_BV(DDB4);            // MISO      -- input

    SPCR = _BV(SPE) | _BV(MSTR); // enable SPI, master mode

    SPSR &= ~_BV(SPI2X);
    switch (clock_divider)
    {
    /* 
         * if 16MHz clock
         * 2 --8MHz  , 4 --4MHz  , 8  --2MHz, 16--1MHz
         * 32--500kHz, 64--250kHz, 128--125kHz
         */
    case 2:
        SPSR |= _BV(SPI2X);
        break;
    case 4:
        break;
    case 8:
        SPSR |= _BV(SPI2X);
        SPCR |= _BV(SPR0);
        break;
    case 16:
        SPCR |= _BV(SPR0);
        break;
    case 32:
        SPSR |= _BV(SPI2X);
        SPCR |= _BV(SPR1);
        break;
    case 64:
        SPCR |= _BV(SPR1);
        break;
    case 128:
        SPCR |= _BV(SPR1) | _BV(SPR0);
        break;
    }
}

void spi_write_REG(volatile uint8_t *port, uint8_t pin, uint8_t regaddr, uint8_t data)
{
    *port &= ~_BV(pin);
    uint8_t buf[2] = {regaddr, data};
    spi_data_multi_RW(buf, 2);
    *port |= _BV(pin);
}

uint8_t spi_read_REG(volatile uint8_t *port, uint8_t pin, uint8_t regaddr)
{
    *port &= ~_BV(pin);
    uint8_t buf[2] = {regaddr | 0x80, 0};
    spi_data_multi_RW(buf, 2);
    *port |= _BV(pin);
    return buf[1];
}

void spi_read_multi_REG(volatile uint8_t *port, uint8_t pin, uint8_t regaddr, uint8_t *buffer, uint8_t count)
{
    *port &= ~_BV(pin);
    spi_data_RW(regaddr | 0x80);
#ifdef SPI_INTERVAL
    _delay_us(SPI_INTERVAL_TIME_US);
#endif
    spi_data_multi_RW(buffer, count);
    *port |= _BV(pin);
}

uint8_t spi_get_errors_count(void)
{
    return spi_errors_count;
}
/**
 * filename : uart.c
 * 
 * created  : 2017/03/07
 * edited   : 2017/03/15
 * edited   : 2018/09/08
 * 
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 * 
 * purpose : uart communication
 */

#include "uart.h"

#include <avr/io.h>
#include <avr/interrupt.h>

#ifdef _UART0PRINT_
#include <math.h>
#endif // _UART0PRINT_

#ifdef _UART0_
volatile uint8_t uart0_tx_buf_head = 0;
volatile uint8_t uart0_tx_buf_tail = 0;
volatile uint8_t uart0_tx_buf[UART0_TX_BUF_SIZE] = {0};
volatile uint8_t uart0_rx_buf_head = 0;
volatile uint8_t uart0_rx_buf_tail = 0;
volatile uint8_t uart0_rx_buf[UART0_RX_BUF_SIZE] ={0};
#endif // _UART0_

#ifdef _UART0_
ISR(USART_RX_vect)
{
    uart0_rx_buf[uart0_rx_buf_head] = UDR0;
    uart0_rx_buf_head = (uart0_rx_buf_head + 1) % UART0_RX_BUF_SIZE;
    if(uart0_rx_buf_head == uart0_rx_buf_tail)
        uart0_rx_buf_tail = (uart0_rx_buf_tail + 1) % UART0_RX_BUF_SIZE;
}

ISR(USART_UDRE_vect)
{
    UDR0 = uart0_tx_buf[uart0_tx_buf_tail];
    uart0_tx_buf_tail = (uart0_tx_buf_tail + 1) % UART0_TX_BUF_SIZE;
    UCSR0A |= _BV(TXC0);
    if(uart0_tx_buf_head == uart0_tx_buf_tail)
        UCSR0B &= ~_BV(UDRIE0);
}

void uart0_begin(uint32_t baud_rate)
{
    UCSR0A = 0;
    UCSR0B = _BV(RXCIE0)|_BV(RXEN0)|_BV(TXEN0);
    UCSR0C = _BV(UCSZ01)|_BV(UCSZ00);
    uint16_t ubrr = (F_CPU/baud_rate/16) - 1;
    UBRR0H = ubrr >> 8;
    UBRR0L = ubrr;

    sei();
}

uint8_t uart0_read(void)
{
    if(uart0_rx_buf_head == uart0_rx_buf_tail) return 0;
    else{
        uint8_t buf = uart0_rx_buf[uart0_rx_buf_tail];
        uart0_rx_buf_tail = (uart0_rx_buf_tail + 1) % UART0_RX_BUF_SIZE;
        return buf;
    }
}

void uart0_write(uint8_t data)
{
    if((uart0_tx_buf_head == uart0_tx_buf_tail) && (UCSR0A & _BV(UDRE0))){
        UDR0 = data;
        UCSR0A |= _BV(TXC0);
        return;
    }
    uart0_tx_buf[uart0_tx_buf_head] = data;
    uart0_tx_buf_head = (uart0_tx_buf_head + 1) % UART0_TX_BUF_SIZE;
    while (uart0_tx_buf_head == uart0_tx_buf_tail){}
    UCSR0B |= _BV(UDRIE0);
}

void uart0_print_str(char *str)
{
    while (*str)
    {
        uart0_write(*str++);
    }
}

uint8_t uart0_available(void)
{
    return (UART0_RX_BUF_SIZE + uart0_rx_buf_head - uart0_rx_buf_tail) % UART0_RX_BUF_SIZE;
}

void uart0_flush(void)
{
    uart0_rx_buf_head = 0;
    uart0_rx_buf_tail = 0;
    uart0_rx_buf[0] = 0;
}

void uart0_print_u16(uint16_t data)
{
    uint16_t temp;
    char buf[6];
    char *str = &buf[5];
    *str = '\0';
    do{
        temp = data / 10;
        char remainder = (data - temp *10) +'0';
        *--str = remainder;
        data = temp;
    }while(data);
    uart0_print_str(str);
}

void uart0_print_i16(int16_t data)
{
    if (data < 0)
    {
        uart0_write('-');
        data = -data;
    }
    uart0_print_u16((uint16_t)data);
}

#ifdef _UART0PRINT_
void uart0_print_u32(uint32_t data)
{
    uint32_t temp;
    char buf[11];
    char *str = &buf[10];
    *str = '\0';
    do{
        temp = data / 10;
        char remainder = (data - temp *10) +'0';
        *--str = remainder;
        data = temp;
    }while(data);
    uart0_print_str(str);
}

void uart0_print_i32(int32_t data)
{
    if (data < 0)
    {
        uart0_write('-');
        data = -data;
    }
    uart0_print_u32((uint32_t)data);
}

void uart0_print_u64(uint64_t data)
{
    uint64_t temp;
    char buf[21];
    char *str = &buf[20];
    *str = '\0';
    do{
        temp = data / 10;
        char remainder = (data - temp *10) +'0';
        *--str = remainder;
        data = temp;
    }while(data);
    uart0_print_str(str);
}

void uart0_print_i64(int64_t data)
{
    if (data < 0)
    {
        uart0_write('-');
        data = -data;
    }
    uart0_print_u64((uint64_t)data);
}

void uart0_print_f(float data, int digits)
{
    if (isnan(data))  // math.h
    {
        uart0_print_str("nan");
        return;
    }
    if (isinf(data))
    {
        uart0_print_str("inf");
        return;
    }
    if (data > 4294967040.0)  // constant determined empirically
    {
        uart0_print_str("ovf");
        return;
    }
    if (data <-4294967040.0)  // constant determined empirically
    {
        uart0_print_str("ovf");
        return;
    }

    // Handle negative numbers
    if (data < 0.0)
    {
        uart0_write('-');
        data = -data;
    }

    uint32_t _int = (uint32_t)data;
    float _dec = data - (float)_int;
    char buf[10];
    char *str = &buf[9];
    *str = '\0';

    if(digits < 5)
    {
        uint16_t temp = 1;
        for (uint8_t i=0; i<digits; ++i) temp *= 10;
        uint16_t _dec_up = (float) (_dec * temp + 0.5);
        if(_dec_up >= temp) ++_int;

        while(digits--)
        {
            temp = _dec_up/10;
            char remainder = (_dec_up - temp *10) +'0';
            *--str = remainder;
            _dec_up = temp;
        }
    }
    else if(digits<10)
    {
        uint32_t temp = 1;
        for (uint8_t i=0; i<digits; ++i) temp *= 10;
        uint32_t _dec_up = (float) (_dec * temp + 0.5);
        if(_dec_up >= temp) ++_int;

        while(digits--)
        {
            temp = _dec_up/10;
            char remainder = (_dec_up - temp *10) +'0';
            *--str = remainder;
            _dec_up = temp;
        }
    }
    else
    {
        // Round correctly so that print(1.999, 2) prints as "2.00"
        float rounding = 0.5;
        for (uint8_t i=0; i<digits; ++i)
        rounding /= 10.0;
        _dec += rounding;

        if (_dec >= 1.0f) ++_int;
        uart0_print_u32(_int);
        uart0_write('.');

        // Extract digits from the remainder one at a time
        while (digits--)
        {
            _dec *= 10.0;
            uint8_t toPrint = (uint8_t)(_dec);
            uart0_write(toPrint + '0');
            _dec -= toPrint;
        }
        return;
    }
    uart0_print_u32(_int);
    uart0_write('.');
    uart0_print_str(str);
}
#endif // _UART0PRINT_
#endif // _UART0_